# scripts/init_redict.sh
#!/usr/bin/env bash
set -x 
set -eo pipefail

# if a redict container is running, print instructions to kill it and exit
RUNNING_CONTAINER=$(docker ps --filter 'name=redict' --format '{{.ID}}')
if [[ -n $RUNNING_CONTAINER ]]; then
    echo >&2 "there is a redict container already runnin, kill it with"
    echo >&2 "    docker kill ${RUNNING_CONTAINER}"
    exit 1
fi

# Launch Redict using Docker
docker run \
    -p "6379:6379" \
    -d \
    --name "redict_$(date '+%s')" \
    registry.redict.io/redict

>$2 echo "Redict is ready to go!"
